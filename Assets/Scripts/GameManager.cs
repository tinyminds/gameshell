﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using GooglePlayGames.BasicApi;

public class GameManager : MonoBehaviour
{

	public GameObject mPlayer;
	public GameObject mEndMenu;
	public GameObject mGameScreen;
	public Button mainMenuButton;
	public Button restartMenuButton;
	public GameObject persistantObj;
	public GameObject bannerObj;
	public Text theScore;
	public Text theCoins;
	public Text levelNum;
	public Text replay;
	public String replayText;
	public String nextLevelText;
	public String endtitleTextWin;
	public String endtitleTextLose;
	public GameObject TitleEndMenuTextObj;
	public GameObject obj3dHolder;
	public GameObject boardHolder;
	// Use this for initialization
	void Awake ()
	{
		persistantObj = GameObject.Find ("persistant");
		bannerObj = GameObject.Find ("AdMobBottomBanner");
		bannerObj.GetComponent<bannerAdmob> ().bannerView.Hide ();
	}

	void Update()
	{
		theScore.text = ""+persistantObj.GetComponent<persistant>().mHits;
		theCoins.text = ""+persistantObj.GetComponent<persistant> ().coinTotal;
		levelNum.text = ""+persistantObj.GetComponent<persistant> ().levelNum;
	}
	public void LateUpdate() {
		if (Input.GetKeyDown(KeyCode.Escape)) {
			GoMainMenu();
		}
	}

	public bool IsMenuShowing {
		get {
			return mEndMenu.activeSelf;
		}
	}

	public bool IsGameShowing {
		get {
			return mGameScreen.activeSelf;
		}
	}

	public void ShowEndMenu ()
	{
		obj3dHolder.SetActive (false);
		if (EventSystem.current.currentSelectedGameObject == null)
		{
			EventSystem.current.SetSelectedGameObject(restartMenuButton.gameObject);
		}
		if (mEndMenu.activeSelf)
		{
			return;
		}
		mGameScreen.SetActive (false);
		mEndMenu.SetActive (true);
		persistantObj.GetComponent<persistant>().Leaderboard(persistantObj.GetComponent<persistant>().firstLoad);
		if (persistantObj.GetComponent<persistant> ().firstLoad == true) {
			persistantObj.GetComponent<persistant> ().firstLoad = false;
		}
		persistantObj.GetComponent<persistant>().ADCountDown = persistantObj.GetComponent<persistant>().ADCountDown +1;
		Debug.Log (persistantObj.GetComponent<persistant> ().ADCountDown + "adverta count");
	}
	public void win()
	{
		ShowEndMenu ();
		persistantObj.GetComponent<persistant> ().coinTotal += 10;
		persistantObj.GetComponent<persistant> ().levelNum += 1;
		replay.text = nextLevelText;
		TitleEndMenuTextObj.GetComponent<Text> ().text = endtitleTextWin;
		//achievements
		if (Social.localUser.authenticated) {
			// Unlock the "welcome" achievement, it is OK to
			// unlock multiple times, only the first time matters.

			//first win achievment
			PlayGamesPlatform.Instance.ReportProgress(
				GPGSIds.achievement_welcome_to_lollygagger,
				100.0f, (bool success) => {
					Debug.Log("(Lollygagger) Welcome Unlock: " +
						success);
					//can't put the coin add in here or it gats called every time, need to find ahievment done callback
					//persistantObj.GetComponent<persistant>().coinTotal += 10;
				});

			// 10 wins achievement
			PlayGamesPlatform.Instance.IncrementAchievement(
				GPGSIds.achievement_sharpshooter,
				1,
				(bool success) => {
					Debug.Log("(Lollygagger) Sharpshooter Increment: " +
						success);
					//persistantObj.GetComponent<persistant>().coinTotal += 10;
				});
			// more wins
			PlayGamesPlatform.Instance.IncrementAchievement(
				GPGSIds.achievement_shoot_more,
				1,
				(bool success) => {
					Debug.Log("(Lollygagger) shoot more Increment: " +
						success);
					//persistantObj.GetComponent<persistant>().coinTotal += 10;
				});
			// evem more wins
			PlayGamesPlatform.Instance.IncrementAchievement(
				GPGSIds.achievement_even_more,
				1,
				(bool success) => {
					Debug.Log("(Lollygagger) shoot even more Increment: " +
						success);
					//persistantObj.GetComponent<persistant>().coinTotal += 10;
				});
		} // end of isAuthenticated
	}

	public void lose()
	{
		ShowEndMenu ();
		replay.text = replayText;
		TitleEndMenuTextObj.GetComponent<Text> ().text = endtitleTextLose;
	}

	public void hint()
	{
		persistantObj.GetComponent<persistant> ().coinTotal -= 10;
		//do hint
	}
	public void addScore()
	{
		IncrementHits();
	}
	public void GoMainMenu ()
	{
		Debug.Log("Going Main Menu!");

		bannerObj.GetComponent<bannerAdmob> ().bannerView.Show ();
		FadeController fader = mPlayer.GetComponentInChildren<FadeController>();
		if (fader != null) {
			fader.GetComponent<FadeController>().FadeToLevel(()=>SceneManager.LoadScene ("MainMenu"));
		}
		else {
			SceneManager.LoadScene("MainMenu");
		}

	}

	public void RestartLevel ()
	{
		boardHolder.GetComponent<floodBoard> ().countDown = 12;
		persistantObj.GetComponent<persistant>().mHits = 0;
		boardHolder.GetComponent<floodBoard> ().destroyTiles ();
		boardHolder.GetComponent<floodBoard> ().hasWon = false;
		boardHolder.GetComponent<floodBoard>().init();
		FadeController fader = mPlayer.GetComponentInChildren<FadeController>();
		if (fader != null) {
			fader.FadeToLevel(()=>{
				mEndMenu.SetActive (false);
				mGameScreen.SetActive (true);
				fader.StartScene();
				obj3dHolder.SetActive (true);
			});
		}
		else {
			mEndMenu.SetActive (false);
			mGameScreen.SetActive (true);
		}

	}

	public void NextLevel ()
	{
		boardHolder.GetComponent<floodBoard> ().countDown = 12;
		persistantObj.GetComponent<persistant>().mHits = 0;
		boardHolder.GetComponent<floodBoard> ().hasWon = false;
		boardHolder.GetComponent<floodBoard>().init();
		FadeController fader = mPlayer.GetComponentInChildren<FadeController>();
		if (fader != null) {
			fader.FadeToLevel(()=>{
				mEndMenu.SetActive (false);
				fader.StartScene();
				obj3dHolder.SetActive (true);
			});
		}
		else {
			mEndMenu.SetActive (false);
			mGameScreen.SetActive (true);
		}
	}


	public void slooshCapsule()
	{
		if(obj3dHolder.GetComponentInChildren<shakeWithPhone>().doShake==true)
		{obj3dHolder.GetComponentInChildren<shakeWithPhone>().doShake = false;}
		else
		{obj3dHolder.GetComponentInChildren<shakeWithPhone>().doShake = true;}
	}

	// do we need hits? but this is what is saved to the leaderboard.
	public void IncrementHits() {
		persistantObj.GetComponent<persistant>().mHits = persistantObj.GetComponent<persistant>().mHits + 1;
	}
}
