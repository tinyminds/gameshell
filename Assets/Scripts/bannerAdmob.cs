﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api; 
using GoogleMobileAds;
public class bannerAdmob : MonoBehaviour {
	public BannerView bannerView;
	public InterstitialAd interstitial;
	private static bool created = false;
	public GameObject persistantObj;
	void Awake()
	{
		if (!created) {
			DontDestroyOnLoad (this.gameObject);
			created = true;
		} 
		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (gameObject);
		}
		persistantObj = GameObject.Find ("persistant");
	}
	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		string appId = "ca-app-pub-6495830595762832~2052839197";
		#elif UNITY_IPHONE
		string appId = "ca-app-pub-3940256099942544~1458002511";
		#else
		string appId = "unexpected_platform";
		#endif

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize(appId);

		this.RequestBanner();
		this.RequestInterstitial ();
	}
		private void RequestBanner()
		{
		Debug.Log ("adddddverty");
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-6495830595762832/2471765304";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-3940256099942544/2934735716";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

		// Create an empty ad request.
		AdRequest request = CreateAdRequest ();
		// Load the banner with the request.
		bannerView.LoadAd(request);
		Debug.Log ("show addddd");
		//adb logcat -s Unity ActivityManager PackageManager dalvikvm DEBUG
		}


		private AdRequest CreateAdRequest()
		{
		return new AdRequest.Builder()
		.AddTestDevice(AdRequest.TestDeviceSimulator)
		.AddTestDevice("7B92467E03D6DC0F79075BDA0A68FD02") //s5 or whatever 2018
		.Build();
		}
	// Update is called once per frame
	void Update () {
		if (persistantObj.GetComponent<persistant> ().ADCountDown == 2) {
			//this.RequestInterstitial ();
			showIntervalBanner ();
			Debug.Log ("show occational ad");
			}
	}

		private void RequestInterstitial()
		{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-6495830595762832/5076504079";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-3940256099942544/4411468910";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		interstitial.OnAdOpening += HandleOnAdOpened;
		// Called when the ad is closed.
		// Create an empty ad request.
		AdRequest request = CreateAdRequest ();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
		}

		public void showIntervalBanner()
		{
		Debug.Log (interstitial.IsLoaded () + "adverta loaded?");
			if (interstitial.IsLoaded()) {
			interstitial.Show();
			Debug.Log ("in adverta");
			}
		persistantObj.GetComponent<persistant> ().ADCountDown = 0;
		}

		public void HandleOnAdOpened(object sender, System.EventArgs bum)
		{
			interstitial.Destroy ();
		}

}
