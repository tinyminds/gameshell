﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialRotate : MonoBehaviour {
	public Camera camera;
	public GameObject startBucket;
	public GameObject endBucket;
	private int rotatePos = 1;
	private float levelSet;
	// Use this for initialization
	void Start () {
			//.GetComponent<LiquidVolumeAnimator> ().fullLevel
	}
	
	// Update is called once per frame
	void Update () {
		//switch rotatePos;
		//on 1 click rotate and set levels to 0.25f;
		//on 2 click rotate and set levels to 0.50f;
		//on 3 click rotate and set levels to 0.75f;
		//on 4 click rotate and set levels to 1f;
		//rotatePos++;
 		if (Input.GetMouseButtonDown(0)){
     		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
    		RaycastHit hit;
     		if (Physics.Raycast(ray, out hit)){
				if (rotatePos < 4) {
					rotatePos++;
				} else {
					rotatePos = 1;
				}
				transform.Rotate (0, -90, 0);
				switch (rotatePos) {
				case 1:
					levelSet = 0.25f;
					break;
				case 2:
					levelSet = 0.50f;
					break;
				case 3:
					levelSet = 0.75f;
					break;
				case 4:
					levelSet = 1f;
					break;
				
				}
				//save this new start button fullness
				//also on next sequence change the dial to reflext amount left - ie if there is only a quarter left the dial is
				//stuck on a quarter, if there are 3/4 left the dial can do 1/4, 1/2 ans 3/4 but 1 is greyed out??? UI
				//dial fills as it turns, full is 1, first quarter is 1/4
				startBucket.GetComponent<levelChange> ().emptyLevel= 1-levelSet;
				//end bucket divide by amount of start buckets? or are the top and bottom the same? overflow?
				endBucket.GetComponent<levelChange> ().fullLevel = levelSet;
     		}
   		}
	}
}
