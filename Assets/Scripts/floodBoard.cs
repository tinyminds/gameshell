﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class floodBoard : MonoBehaviour {

	public GameObject tileObject;
	public int useColorNum = 0;
	public Color[] colors;
	public GameObject[] colorButtons;
	private GameObject[,] tilesArray = new GameObject[10, 7];
	private int numRows = 10;
	private int numCols = 7;
	private List<Vector2> filled;
	public Camera cam;
	public bool hasWon = false;
	public GameObject gameManager;
	public GameObject topLeftTile;
	public int countDown = 10;
	public Text countDownText;
	public float force = 10f;
	public float forceOffset = 0.1f;
	//CrumpleMesh crump = new CrumpleMesh;
	//[System.Serializable]
	//public class ColourSet
	//{
	//	public Color theColor;
	//}
	//public ColourSet[] colourSets;

	// Use this for initialization
	void Start () {
		init ();
	}

	public void init(){
		Debug.Log ("do inti");
		hasWon = false;
		for(int i = 0; i<colors.Length; i++)
		{
			Color tempCol = colors [i];
			tempCol.a = 1f;
			colorButtons[i].GetComponent<Image> ().color = tempCol;
		}
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++){
				GameObject tile = Instantiate(tileObject);
				if (i == 0 && j == 0) {
					topLeftTile = tile;
				}
				tile.transform.parent = this.transform;
				float yPos = 4.5f - 1f * i;
				float xPos = -3f + 1f * j;
				float rotx = tile.transform.localRotation.x + i + j;
				float roty = tile.transform.localRotation.y + i;
				float rotz = tile.transform.localRotation.z + j;
				tile.transform.localRotation = new Quaternion (rotx, roty, rotz, 0);
				tile.transform.position = new Vector3 (xPos, yPos, 10f);
				int tileColor = Random.Range (0, colors.Length);
				tile.GetComponent<tileScript> ().value = tileColor;
				tile.GetComponent<tileScript> ().coordinate = new Vector2 (j, i);
				tile.GetComponent<Renderer> ().material.color = colors [tileColor];
				tilesArray [i,j] = tile;
			}
		}
	}
	void Update () {
		countDownText.text = ""+countDown+"";
		if ((countDown == 0)&&(!hasWon) ){
			StartCoroutine(pauseIt(false));
		}
		if (hasWon) {
			//pause, do animation
			StartCoroutine(pauseIt(true));
			//gameManager.GetComponent<GameManager> ().win ();
		}
		//if (Input.GetButtonDown ("Fire1")) {
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)){
				MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
				if (deformer) {
					Vector3 point = hit.point;
					point += hit.normal * forceOffset;
					deformer.AddDeformingForce(point, force);
				}
				//filled = new List<Vector2> ();
				//floodFill (hit.transform.GetComponent<tileScript> ().coordinate, hit.transform.GetComponent<tileScript> ().value);
				//Debug.Log (filled.Count);
				//	for (int i = 0; i < filled.Count; i++) {
				//	tilesArray [(int)filled [i].y, (int)filled [i].x].GetComponent<Renderer> ().material.color = colors[useColorNum];
				//	tilesArray [(int)filled [i].y, (int)filled [i].x].GetComponent<tileScript> ().value = useColorNum;
				//	}
				//hasWon = checkIfWon ();
				}
		//}
	}


	IEnumerator pauseIt(bool hasWon)
	{
		yield return new WaitForSeconds(3f);
		if(hasWon)
		{
		gameManager.GetComponent<GameManager> ().win ();
		}
		else
		{
			gameManager.GetComponent<GameManager> ().lose ();
		}
	}
	 bool checkIfWon (){
		int[] testThem = new int[numRows*numCols];
		int count = 0;
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				testThem[count] = tilesArray [i, j].GetComponent<tileScript> ().value;
				count++;
			}
		}
		return bruteforce (testThem);
	}

	public void destroyTiles (){
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++) {
				Destroy (tilesArray [i, j]);
			}
		}
	}

	public static bool bruteforce(int[] inputNum) {
     for(int i = 1; i < inputNum.Length; i++) {
         if(inputNum[0] != inputNum[i]) return false;
     }
     return true;
	}

	void floodFill(Vector2 p, int n){
		if (p.x < 0 || p.y < 0 || p.x > numCols - 1 || p.y > numRows - 1) {
			return;
		}
		if (tilesArray [(int)p.y, (int)p.x] != null && tilesArray [(int)p.y, (int)p.x].GetComponent<tileScript> ().value == n && !filled.Contains(p)) {
			filled.Add (p);
			floodFill (new Vector2 (p.x + 1, p.y),n);
			floodFill (new Vector2 (p.x - 1, p.y),n);
			floodFill (new Vector2 (p.x, p.y + 1),n);
			floodFill (new Vector2 (p.x, p.y - 1),n);
		}

	}

	public void setUseColour (int useCol){
		//mark which one is selected
		if (topLeftTile.GetComponent<tileScript> ().value == useCol) {
			Debug.Log ("same colour");
		} else {
			useColorNum = useCol;
			filled = new List<Vector2> ();
			floodFill (topLeftTile.GetComponent<tileScript> ().coordinate, topLeftTile.GetComponent<tileScript> ().value);
			for (int i = 0; i < filled.Count; i++) {
				tilesArray [(int)filled [i].y, (int)filled [i].x].GetComponent<Renderer> ().material.color = colors [useColorNum];
				tilesArray [(int)filled [i].y, (int)filled [i].x].GetComponent<tileScript> ().value = useColorNum;
			}
			//check all the connected ones and add effect grouping them, her it is turn on crumplemesh but could be something else
			filled = new List<Vector2> ();
			floodFill (topLeftTile.GetComponent<tileScript> ().coordinate, topLeftTile.GetComponent<tileScript> ().value);
			for (int i = 0; i < filled.Count; i++) {
				//tilesArray [(int)filled [i].y, (int)filled [i].x].GetComponent<CrumpleMesh> ().enabled = true;
				tilesArray [(int)filled [i].y, (int)filled [i].x].transform.localRotation = new Quaternion (0, 0, 0, 0);
			}
			hasWon = checkIfWon ();
			countDown--;

		}
	}
}