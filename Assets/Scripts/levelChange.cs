﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelChange : MonoBehaviour {
	public GameObject theLevelObj;
	public bool isTriggered = false;
	public float fullLevel = 1f;
	public float emptyLevel = 0.1f;
	public bool isPouring = true;
	public GameObject nextTube;
	public float timInterval = 1f;
	private float catchLerp = 0.05f;
	private float _timeStartedLerping;
	public GameObject triggerParticles;
	public bool isEndBucket = false;
	public bool changeColour = false;
	public Color newColor;
	public Color newTopColor;
	public float PauseBeforeDrain = 0f;

	void Start () {
	}

	void Update () {
		if (changeColour) {
			theLevelObj.GetComponent<LiquidVolumeAnimator> ().mats [0].SetColor ("_Color", newColor);
			theLevelObj.GetComponent<LiquidVolumeAnimator> ().mats [0].SetColor ("_EmissionColor", newColor);
			//theLevelObj.GetComponent<LiquidVolumeAnimator> ().mats [0].SetColor ("_SColor", newColor);
			theLevelObj.GetComponent<LiquidVolumeAnimator> ().mats [0].SetColor ("_SEmissionColor", newTopColor);
		}

		if (isTriggered) {
			float timeSinceStarted = Time.time - _timeStartedLerping;
            float percentageComplete = timeSinceStarted / timInterval;
			if (isPouring) {
				//Debug.Log (theLevelObj.GetComponent<LiquidVolumeAnimator> ().level);
				theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = Mathf.Lerp (emptyLevel, fullLevel, percentageComplete);
				//Debug.Log ("is pouring" + theLevelObj.GetComponent<LiquidVolumeAnimator> ().level+ " - " + theLevelObj.name);
				if ((Mathf.Round(theLevelObj.GetComponent<LiquidVolumeAnimator> ().level*100)/100) >= (fullLevel-catchLerp)) {
					theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = fullLevel;
					//Debug.Log ("finished pouring " + theLevelObj.name);
					_timeStartedLerping = 0;
					triggerNextTube ();
					//if(!isEndBucket){drainDown ();}
					StartCoroutine(pauseIt());
				}
			} else {
				theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = Mathf.Lerp (fullLevel, emptyLevel, percentageComplete);
				//Debug.Log ("is draining" + theLevelObj.GetComponent<LiquidVolumeAnimator> ().level + " - " + theLevelObj.name);
				if ((Mathf.Round(theLevelObj.GetComponent<LiquidVolumeAnimator> ().level*100)/100) <= (catchLerp-emptyLevel)) {
					theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = emptyLevel;
					//Debug.Log ("stoped draining !!!" + theLevelObj.name);
					_timeStartedLerping = 0;
					isTriggered = false;
					if(triggerParticles)
					{
						triggerParticles.SetActive(false);
					}
				}
			}
		}
	}

	IEnumerator pauseIt()
	{
		if (!isEndBucket) {
			isPouring = false;
			theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = fullLevel;
			theLevelObj.GetComponent<LiquidVolumeAnimator> ().GravityDirection = new Vector3 (0, -10f, 0);
		}
		yield return new WaitForSeconds(PauseBeforeDrain);
		if(!isEndBucket){drainDown ();}
	}

	void drainDown()
	{
		//Debug.Log ("start to drain");
		//isPouring = false;
		//theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = fullLevel;
		//theLevelObj.GetComponent<LiquidVolumeAnimator> ().GravityDirection = new Vector3(0,-10f,0);
		isTriggered = true;
		if(triggerParticles)
		{
			if (changeColour) {
				triggerParticles.GetComponent<ParticleSystemRenderer> ().material.SetColor ("_TintColor", newTopColor);
			}
			triggerParticles.SetActive(true);
		}
		_timeStartedLerping = Time.time;
	}

	public void setTimeLerp(){
		_timeStartedLerping = Time.time;
	}

	void triggerNextTube()
	{
		isTriggered = false;
		//Debug.Log ("call trigger next tube");
			if (nextTube) {
				theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = emptyLevel;
				nextTube.GetComponent<levelChange> ().isTriggered = true;
				nextTube.GetComponent<levelChange> ()._timeStartedLerping = Time.time;
			}
	}

	public void setPouringBool(bool setIsPouring)
	{
		isPouring = setIsPouring;
	}

	public void setTriggeredBool(bool setIsTriggered)
	{
		isTriggered = setIsTriggered;
	}

	public void changeGravityPour()
	{
		theLevelObj.GetComponent<LiquidVolumeAnimator> ().GravityDirection = new Vector3(0,10f,0);
	}

	public void changeGravityEmpty()
	{
		theLevelObj.GetComponent<LiquidVolumeAnimator> ().GravityDirection = new Vector3(0,-10f,0);
	}

	public void resetToFull(){
		theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = fullLevel;
		changeGravityEmpty ();
		isTriggered = false;
		isPouring = false;
	}

	public void resetToEmpty(){
		theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = emptyLevel;
		changeGravityPour ();
		isTriggered = false;
		isPouring = true;
	}
	public void resetToEmptyBucket(){
		theLevelObj.GetComponent<LiquidVolumeAnimator> ().level = -0.1f;
		changeGravityEmpty ();
		isTriggered = false;
		isPouring = true;
	}


}
