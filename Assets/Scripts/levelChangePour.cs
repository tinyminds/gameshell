﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelChangePour : MonoBehaviour {
	public GameObject theLevelObj;
	public bool isTriggered = false;
	private float fullLevel = 1f;
	private float emptyLevel = 0f;
	private static float t = 0f;
	private int multiplierVal = 100;
	public bool isUp = true;
	public GameObject nextTubeDown;
	public GameObject nextTubeUp;
	public float timInterval = 0.2f;

	void Start () {
		
	}

	void Update () {
		if (isTriggered) {
				if (isUp) {
					Debug.Log ("going up" + t+ "t "+theLevelObj.GetComponent<LiquidVolumeAnimatorPour> ().level);
					theLevelObj.GetComponent<LiquidVolumeAnimatorPour> ().level = (Mathf.Lerp ((emptyLevel * multiplierVal), (fullLevel * multiplierVal), t) / multiplierVal);
					t += timInterval * Time.deltaTime;
					if (theLevelObj.GetComponent<LiquidVolumeAnimatorPour> ().level == fullLevel) {
						//Debug.Log ("filled to the top");
						t = 0f;
						triggerNextTube ();
					}	
				} else {
					Debug.Log ("going down" + t + "t "+theLevelObj.GetComponent<LiquidVolumeAnimatorPour> ().level);
					t += timInterval * Time.deltaTime;	
					theLevelObj.GetComponent<LiquidVolumeAnimatorPour> ().level = (Mathf.Lerp ((fullLevel * multiplierVal), (emptyLevel * multiplierVal), t) / multiplierVal);
					if (theLevelObj.GetComponent<LiquidVolumeAnimatorPour> ().level == emptyLevel) {
						//Debug.Log ("empty to the bottom");
						t = 0f;
						triggerNextTube ();
					}
				}
		}
	}


	void triggerNextTube()
	{
		isTriggered = false;
		Debug.Log ("call trigger next tube");
		if (!isUp) {
			if (nextTubeDown) {
				//Debug.Log ("next tube to down trigger exists");
				nextTubeDown.GetComponent<levelChange> ().isTriggered = true;
			}
		} else {
			if (nextTubeUp) {
				//Debug.Log ("next tube to up trigger exists");
				nextTubeUp.GetComponent<levelChange> ().isTriggered = true;
			}
		}
	}
	public void setUpBool(bool setIsUp)
	{
		//this is just changing one to up, probably not very useful
		isUp = setIsUp;
	}

	public void setTriggeredBool(bool setIsTriggered)
	{
		isTriggered = setIsTriggered;
	}

	public void resetToFull(){
		theLevelObj.GetComponent<LiquidVolumeAnimatorPour> ().level = 1f;
		isTriggered = false;
		isUp = true;
	}
}
