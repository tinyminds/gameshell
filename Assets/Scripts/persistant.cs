﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using GooglePlayGames.BasicApi;
using UnityEngine.UI;
using UnityEngine.Advertisements;


public class persistant : MonoBehaviour {
	public int coinTotal;
	private Text showScore;
	public int mHits;
	public int levelNum;
	public int ADCountDown = 0;
	public int updatedCoins;
	public int updatedLevels;
	public bool firstLoad = true;
	// Use this for initialization
	void Start () {
		
	}
	private static bool created = false;

	void Awake()
	{
		if (!created)
		{
			DontDestroyOnLoad(this.gameObject);
			created = true;
			Debug.Log("Awake: " + this.gameObject);
		}
		if (FindObjectsOfType (GetType ()).Length > 1) {
			Destroy (gameObject);
		}
	}

	public void Leaderboard(bool firstLoad)
	{
		// Submit leaderboard scores, if authenticated
		if (PlayGamesPlatform.Instance.localUser.authenticated)
		{
			// Note: make sure to add 'using GooglePlayGames'
			PlayGamesPlatform.Instance.ReportScore(mHits,
				GPGSIds.leaderboard_targets_hit_in_one_level,
				(bool success) =>
				{
					Debug.Log("(Lollygagger) Leaderboard update success: " + success);
				});
			//and update coin total and level number reached.
			WriteUpdatedScore(firstLoad);
		}
	}

	// Update is called once per frame
	void Update () {
		
	}

	//put this stuff in persistant and save other shit too
	public void WriteUpdatedScore(bool firstLoad) {
		// Local variable
		ISavedGameMetadata currentGame = null;

		// CALLBACK: Handle the result of a write
		Action<SavedGameRequestStatus, ISavedGameMetadata> writeCallback = 
			(SavedGameRequestStatus status, ISavedGameMetadata game) => {
			Debug.Log("(Lollygagger) Saved Game Write: " + status.ToString());
		};

		// CALLBACK: Handle the result of a binary read
		Action<SavedGameRequestStatus, byte[]> readBinaryCallback = 
			(SavedGameRequestStatus status, byte[] data) => {
			Debug.Log("(Lollygagger) Saved Game Binary Read: " + status.ToString());
			if (status == SavedGameRequestStatus.Success) {
				// Read score from the Saved Game
				int level = 0;
				int coins = 0;
				try {
					string scoreString = System.Text.Encoding.UTF8.GetString(data);
					Debug.Log("pulled down data:" + data);
					Debug.Log(scoreString + "poop");
					Debug.Log("pulled down BUM:" + System.Text.Encoding.UTF8.GetString(data));
					level = Convert.ToInt32(scoreString.Split(":"[0])[0]);
					coins = Convert.ToInt32(scoreString.Split(":"[0])[1]);
				} catch (Exception e) {
					Debug.Log("(Lollygagger) Saved Game Write: convert exception");
				}

				// Increment score, convert to byte[]
				int levelnum = level;
				int newCoins = coins;
				if(firstLoad)
				{
				//on first load grab the score from the saved game and stuff it into the local
				//then write the local back to the savedate
				//on not first load just write the current local to the save data
				coinTotal += coins;
				levelNum += levelnum;
					Debug.Log("in first load");
				}
				string newScoreString = Convert.ToString(levelNum+":"+coinTotal);
				byte[] newData = System.Text.Encoding.UTF8.GetBytes(newScoreString);

				// Write new data
				Debug.Log("(Lollygagger) New Score levelnum Fart: " + levelNum.ToString());
				Debug.Log("(Lollygagger) save string: " + newScoreString.ToString());
				WriteSavedGame(currentGame, newData, writeCallback);
			}
		};

		// CALLBACK: Handle the result of a read, which should return metadata
		Action<SavedGameRequestStatus, ISavedGameMetadata> readCallback = 
			(SavedGameRequestStatus status, ISavedGameMetadata game) => {
			Debug.Log("(Lollygagger) Saved Game Read: " + status.ToString());
			if (status == SavedGameRequestStatus.Success) {
				// Read the binary game data
				currentGame = game;
				PlayGamesPlatform.Instance.SavedGame.ReadBinaryData(game, 
					readBinaryCallback);
			}
		};

		// Read the current data and kick off the callback chain
		Debug.Log("(Lollygagger) Saved Game: Reading");
		ReadSavedGame("file_total_hits", readCallback);
	}


	public void ReadSavedGame(string filename, 
		Action<SavedGameRequestStatus, ISavedGameMetadata> callback) {

		ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
		savedGameClient.OpenWithAutomaticConflictResolution(
			filename, 
			DataSource.ReadCacheOrNetwork, 
			ConflictResolutionStrategy.UseLongestPlaytime, 
			callback);
	}

	public void WriteSavedGame(ISavedGameMetadata game, byte[] savedData, 
		Action<SavedGameRequestStatus, ISavedGameMetadata> callback) {

		SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder()
			.WithUpdatedPlayedTime(TimeSpan.FromMinutes(game.TotalTimePlayed.Minutes + 1))
			.WithUpdatedDescription("Saved at: " + System.DateTime.Now);

		// You can add an image to saved game data (such as as screenshot)
		// byte[] pngData = <PNG AS BYTES>;
		// builder = builder.WithUpdatedPngCoverImage(pngData);

		SavedGameMetadataUpdate updatedMetadata = builder.Build();

		ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
		savedGameClient.CommitUpdate(game, updatedMetadata, savedData, callback);
	}


	/*public void pseudo_do_achievemnets(){
	if (Social.localUser.authenticated) {
		// Unlock the "welcome" achievement, it is OK to
		// unlock multiple times, only the first time matters.
		PlayGamesPlatform.Instance.ReportProgress(
			GPGSIds.achievement_welcome_to_lollygagger,
			100.0f, (bool success) => {
				Debug.Log("(Lollygagger) Welcome Unlock: " +
					success);
			});

		// Increment the "sharpshooter" achievement
		PlayGamesPlatform.Instance.IncrementAchievement(
			GPGSIds.achievement_sharpshooter,
			1,
			(bool success) => {
				Debug.Log("(Lollygagger) Sharpshooter Increment: " +
					success);
			});
		}*/
		

	//show normal ad
		 public void ShowAd()
        {
            if (Advertisement.IsReady())
            {
                Advertisement.Show();
            }
        }

        //show reward ads
    public void ShowRewardedAd()
    {
		Debug.Log ("clicked butt");    
			if (Advertisement.IsReady("rewardedVideo"))
            {
                var options = new ShowOptions { resultCallback = HandleShowResult };
                Advertisement.Show("rewardedVideo", options);
				Debug.Log("reward unity");
            }
            else
            {
                Debug.Log("Ads not ready");
            }
    }


    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
			//put popup saying here's your reward
                Debug.Log("The ad was successfully shown.");

    /*here we give 50 poinst as reward*/
                coinTotal += 15;
                break;
            case ShowResult.Skipped:
			//popup saying skipped
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
			//popup saying oopey
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
