﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rndomRot : MonoBehaviour {
	public float rotSpeed = 30.0f;
	private Vector3 randomRot;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public Vector3 GetRandomRotation()
	{
		randomRot = (Random.insideUnitSphere + Vector3.forward).normalized;
		return randomRot;
	}

	public void randyRot()
	{
		transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (GetRandomRotation (), (transform.up + Vector3.up).normalized), rotSpeed * Time.deltaTime);
	}
}
