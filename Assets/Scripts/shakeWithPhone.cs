﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shakeWithPhone : MonoBehaviour {
	public float x;
	public float y;
	public float z;
	public Quaternion startPos;
	public Quaternion temp;
	public bool doShake = false;
	// Use this for initialization
	void Start () {
		Input.gyro.enabled = true;
		startPos = this.transform.rotation;
	}

	// Update is called once per frame
	void Update () {
		if (doShake) {
			x = Input.gyro.rotationRate.x / 2;
			y = Input.gyro.rotationRate.y / 2;
			z = Input.gyro.rotationRate.z / 2;
			temp = new Quaternion (x, y, z, 1);
			//Debug.Log ("bum" + temp);
			this.transform.rotation = temp;
		} else {
			this.transform.rotation = startPos;
		}
	}
		
}
