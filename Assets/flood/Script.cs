﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Script : MonoBehaviour {

	public GameObject tileObject;
	public int useColorNum = 0;
	public Color[] colors;
	public GameObject[] colorButtons;
	private GameObject[,] tilesArray = new GameObject[10, 7];
	private int numRows = 10;
	private int numCols = 7;
	private List<Vector2> filled;
	public Camera cam;
//FF2D007C
//04A10F7C
//0000FF7C
//FBFF007C
	//[System.Serializable]
	//public class ColourSet
	//{
	//	public Color theColor;
	//}
	//public ColourSet[] colourSets;

	// Use this for initialization
	void Start () {
		for(int i = 0; i<colors.Length; i++)
		{
			Color tempCol = colors [i];
			tempCol.a = 1f;
			colorButtons[i].GetComponent<Image> ().color = tempCol;
		}
		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols; j++){
				GameObject tile = Instantiate(tileObject);
				tile.transform.parent = this.transform;
				float yPos = 4.5f - 1f * i;
				float xPos = -3f + 1f * j;
				tile.transform.position = new Vector3 (xPos, yPos, 10f);
				int tileColor = Random.Range (0, colors.Length);
				tile.GetComponent<tileScript> ().value = tileColor;
				tile.GetComponent<tileScript> ().coordinate = new Vector2 (j, i);
				tile.GetComponent<Renderer> ().material.color = colors [tileColor];
				tilesArray [i,j] = tile;
			}
		}
	}

	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)){
				filled = new List<Vector2> ();
				floodFill (hit.transform.GetComponent<tileScript> ().coordinate, hit.transform.GetComponent<tileScript> ().value);
					for (int i = 0; i < filled.Count; i++) {
					tilesArray [(int)filled [i].y, (int)filled [i].x].GetComponent<Renderer> ().material.color = colors[useColorNum];
					tilesArray [(int)filled [i].y, (int)filled [i].x].GetComponent<tileScript> ().value = useColorNum;
					}
				}
		}
	}

	void floodFill(Vector2 p, int n){
		if (p.x < 0 || p.y < 0 || p.x > numCols - 1 || p.y > numRows - 1) {
			return;
		}
		if (tilesArray [(int)p.y, (int)p.x] != null && tilesArray [(int)p.y, (int)p.x].GetComponent<tileScript> ().value == n && !filled.Contains(p)) {
			filled.Add (p);
			floodFill (new Vector2 (p.x + 1, p.y),n);
			floodFill (new Vector2 (p.x - 1, p.y),n);
			floodFill (new Vector2 (p.x, p.y + 1),n);
			floodFill (new Vector2 (p.x, p.y - 1),n);
		}

	}

	public void setUseColour (int useCol){
		useColorNum = useCol;
	}
}